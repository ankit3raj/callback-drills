/*
    Q1. Create a new file data.json
    { 
        "max": {
            colors: ['Orange', 'Red']
        }
    }


    Q2. Copy the contents of the data.json into a new folder output/data.json and delete the original file.
    Q3. Write a function that takes a person name and fav color as parameters and writes to data.json file
    (Do not replace the old content).
    Q4. Write a function that takes a person name and fav hobby as param and add that hobby as a separate key and write to data.json.
    (Do not replace the old content).
*/

const fs = require("fs");
const path = require("path");
//1 

// let data = {
//   max: {
//     colors: ["Orange", "Red"],
//   },
// };

// fs.writeFile(
//   "./data.json",
//   JSON.stringify(data),
//   { encoding: "utf8" },
//   (err) => {
//     if (err) console.log(err);
//     else {
//       console.log("File written successfully\n");
//     }
//   }
// );


//2

// fs.readFile(
//   path.join(__dirname, "./data.json"),
//    "utf8" ,
//   (err,data) => {
//     // Display the file content
//     if(err){
//       console.log(err);
//     }else{
//      fs.writeFile(
//   "./output/data.json",data,
  
//   { encoding: "utf8" },
//   (err) => {
//     if (err) console.log(err);
//     else {
//       console.log("File written successfully\n");
//     }
//   }
// );
//     }
//   }
// );

//3 

function addObject(data, name, colors){
  let obj = {...data};
  obj[name] = {color:[colors]}
  
  return obj;
}




// fs.readFile(
//   path.join(__dirname, "./output/data.json"),
//    "utf8" ,
//   (err,data) => {
//     // Display the file content
//     if(err){
//       console.log(err);
//     }else{
//       const newData = addObject(JSON.parse(data), 'payne',['blue','yellow']);
//      fs.writeFile(
//   "./output/data.json",JSON.stringify(newData),
  
//   { encoding: "utf8" },
//   (err) => {
//     if (err) console.log(err);
//     else {
//       console.log("File written successfully\n");
//     }
//   }
// );
//     }
//   }
// );

//4 


function addProperty(data,name,hobbies){
  const obj={...data};
  obj[name] = {hobby:hobbies}
  return obj;
}


fs.readFile(
  path.join(__dirname, "./output/data.json"),
   "utf8" ,
  (err,data) => {
    // Display the file content
    if(err){
      console.log(err);
    }else{
      const newHobby = addProperty(JSON.parse(data), 'payne',['Music']);
     fs.writeFile(
  "./output/data.json",JSON.stringify(newHobby),
  
  { encoding: "utf8" },
  (err) => {
    if (err) console.log(err);
    else {
      console.log("File written successfully\n");
    }
  }
);
    }
  }
);





